# SPA Personas

WebApp tipo SPA que permite cargar datos de personas desde un formulario HTML. Estos son mapeados a un modelo, para luego ser insertados en un listado de la interfaz.

Entre las operaciones que se realizan con el front-end, se encuentran agregar/borrar objetos y la administración de avisos.

No se encuentra integrada ninguna conexión con una base de datos, así como tampoco se persisten los datos.