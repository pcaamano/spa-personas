// Modelo del Objeto
class Person{
    constructor(name,mail,tel,direction){
        this.name = name;
        this.mail = mail;
        this.tel = tel;
        this.direction = direction;
    }
}

// Metodos Interfaz
class AppPanel{
    addPerson(person){
        
        // se accede al panel donde se van a mostrar los objetos
        const personsList = document.getElementById('resultList');
        // se crea elemento HTML
        const personItem = document.createElement('div');
        personItem.innerHTML = `
            <div class="person-item card">
                <div class="person-header">
                    <h2>${person.name}</h2>
                    <a name="delete" class="delete-button">Delete</a>
                </div>
                <p>Correo: ${person.mail}</p>
                <p>Telefono: ${person.tel}</p>
                <p>Direccion: ${person.direction}</p>
            </div>
        `;
        personsList.appendChild(personItem);
    }

    removePerson(element){
        if(element.name === 'delete'){
            element.parentElement.parentElement.remove();
        }
    }

    clearForm(){
        document.getElementById('objectForm').reset();
    }

    actionMessage(className,message){
        if(className === 'error' || className === 'info'){
            const popup = document.createElement('div');
            popup.innerHTML = `
                <div class="popup ${ className }">
                    ${message}
                </div>
            `;
            document.body.appendChild(popup);
            setTimeout( () =>{
                document.querySelector('.popup').remove();
            }, 2000);
        }
    }
}

/**  EVENTOS DE LA APP **/

/* Enviar Formulario */
document.getElementById('objectForm')
    .addEventListener('submit',(event)=>{
        // obtener datos y guardarlos en constantes
        const name = document.getElementById('name').value;
        const mail = document.getElementById('mail').value;
        const tel = document.getElementById('tel').value;
        const direction = document.getElementById('direction').value;

        /** Se valida que el nombre no este vacio **/
        if(name!=''){
            // se instancia objeto persona con datos obtenidos
            const person = new Person(name, mail, tel, direction);

            // Se intancia la interfaz
            const appPanel = new AppPanel();
            // Se invoca metodo  para agregar elementos y se le pasa datos capturados
            appPanel.addPerson(person);
            // Se limpian los datos del formulario
            appPanel.clearForm();
            // Se invoca metodo para mostrar popup
            appPanel.actionMessage('info','¡Datos cargados!');
        }else{
            // Se intancia la interfaz
            const appPanel = new AppPanel();
            // Se invoca metodo para mostrar popup
            appPanel.actionMessage('error', 'Ingrese nombre por favor...');
        }
        
        event.preventDefault();//prevenir que se refresque la app
    }
);

// Se captura cualquier click del listado de objetos
document.getElementById('resultList').addEventListener('click', (event) => {
    // Instanciacion de la interfaz
    const appPanel = new AppPanel();
    // Se invoca metodo para borrar elementos
    appPanel.removePerson(event.target);
    // Se invoca metodo para mostrar popup
    appPanel.actionMessage('info', '¡Datos borrados!');
});